AWSTemplateFormatVersion: 2010-09-09
Description: A Cloudformation Script to demonstrate Task defination

Parameters:
  environment:
    Description: env
    Type: String
    Default: dev
  ProductName:
    Description: Parent Product name.
    Type: String
    Default: cyberkeeda
  ProjectName:
    Description: Project Name
    Type: String
    Default: cyberkeeda
  CNEnvironment:
    Description: The equivalent CN name of the environment being worked on
    Type: String
    AllowedValues:
      - dev
      - prv
      - stg
  Region:
    Description: ck Region specific parameter
    Type: String
    AllowedValues:
      - day
      - iad
  DestinationS3Bucket:
    Description: ck S3 Bucket name to copy sftp downloaded files.
    Type: String
    Default: ck-devops-dev-artifact-bucket
  FilePrefix:
    Description: File prefix to be used to match regex.
    Type: String
    Default: pod
  SFTPHostFQDN:
    Description: Remote SFTP Host FQDN.
    Type: String
    Default: 65.0.131.41
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})"
    ConstraintDescription : "must be a valid IP CIDR range of the form x.x.x.x"
  SFTPHostPort:
    Description: Remote SFTP Host Port.
    Type: String
    Default: 22
  SFTPUserName:
    Description: Remote SFTP Host username.
    Type: String
    Default: sftpadmin
  SFTPPasswordParameterStoreName:
    Description: Remote SFTP Host Parameter store name.
    Type: String
    Default: sftppass
  RemoteSFTPFileLocation:
    Description: Remote SFTP default landing directory.
    Type: String
    Default: "/home/sftpadmin/"
  ReprocessFileList:
    Description: Individual file names seperated.
    Type: String
    Default: None
  ContainerImageUrlwithTag:
    Description: Container Image URL with tag.
    Type: String
    Default: docker.io/jackuna/sftpnew
  SFTPJobType:
    Description: SFTP File Process Type
    Type: String
    AllowedValues:
      - upload
      - reupload
    Default: upload
  ECSClusterARN:
    Description: ECS Cluster ARN to schedule Task 
    Type: String
    Default: arn:aws:ecs:ap-south-1:897248824142:cluster/sftp

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - 
        Label:
          default: ck Environment Details
        Parameters:
          - ProductName
          - ProjectName
          - CNEnvironment
          - Region
      - 
        Label:
          default: Remote SFTP Server details used as Container Environment Variables.
        Parameters:
          - SFTPHostFQDN
          - SFTPHostPort
          - SFTPUserName
          - SFTPPasswordParameterStoreName
          - RemoteSFTPFileLocation
          - SFTPJobType
      -
        Label:
          default: Other Conatiner Environment Variables
        Parameters:
          - DestinationS3Bucket
          - FilePrefix
          - ReprocessFileList 
      
Resources:
  ExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Sub "${ProductName}-${Region}-${ProjectName}-role"
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: [ 'ecs-tasks.amazonaws.com', 'events.amazonaws.com' ]
            Action: sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy
      Policies:
      - PolicyName: !Sub "${ProductName}-${Region}-${ProjectName}-role-inlinePolicy"
        PolicyDocument: 
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                - s3:ListBucketMultipartUploads
                - s3:ListBucket
                - s3:GetBucketAcl
                - s3:GetBucketPolicy
                - s3:ListMultipartUploadParts
                - s3:PutObject
                - s3:GetObjectAcl
                - s3:GetObject
                - s3:PutObjectAcl
                Resource:
                - !Sub "arn:aws:s3:::${DestinationS3Bucket}*"
              - Effect: Allow
                Action:
                - ssm:GetParameters
                Resource:
                - !Sub "arn:aws:ssm:ap-south-1:897248824142:parameter/${SFTPPasswordParameterStoreName}"
              - Effect: Allow
                Action:
                - ecs:RunTask
                Resource:
                - !Sub "arn:aws:ecs:ap-south-1:897248824142:task-definition/${ProductName}-${Region}-${ProjectName}-ecs-task:*"
              - Effect: Allow
                Action: iam:PassRole
                Resource:
                - "*"
                Condition:
                  StringLike:
                    iam:PassedToService: ecs-tasks.amazonaws.com


  TaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: !Sub "${ProductName}-${Region}-${ProjectName}-ecs-task"
      Memory: 256
      NetworkMode: bridge 
      ExecutionRoleArn: !Ref ExecutionRole
      TaskRoleArn : !Ref ExecutionRole
      ContainerDefinitions:
        - Name: !Sub "${ProductName}-${Region}-${ProjectName}-container"
          Image: !Ref ContainerImageUrlwithTag
          Memory: 128
          Cpu: 0
          MountPoints: 
            - 
              SourceVolume: "ecs-logs"
              ContainerPath: "/var/log/ecs"
          Command: 
            - python
            - sftp_python.py
          WorkingDirectory: "/usr/local/BornTogetDeleted"
          Secrets:
            - 
              Name: SFTP_PASSWORD
              ValueFrom: !Ref SFTPPasswordParameterStoreName
          Environment: 
            - 
              Name: APPLICATION_LOGS
              Value: !Sub  "/var/log/ecs/${ProductName}-${Region}-${ProjectName}-ecs-task.logs"
            - 
              Name: DESTINATION_S3_BUCKET
              Value: !Ref DestinationS3Bucket
            - 
              Name: FILE_PREFIX
              Value: !Ref FilePrefix
            - 
              Name: SFTP_HOST
              Value: !Ref SFTPHostFQDN
            - 
              Name: JOB_TYPE
              Value: !Ref SFTPJobType
            - 
              Name: LOCAL_PUT_LOCATION
              Value: "/tmp/"
            - 
              Name: OLD_FILE_PREFIX
              Value: "pod062821"
            - 
              Name: SFTP_PORT
              Value: !Ref SFTPHostPort
            - 
              Name: REMOTE_SFTP_LOCATION
              Value: !Ref RemoteSFTPFileLocation
            - 
              Name: REPROCESS_FILE_LIST
              Value: !Ref ReprocessFileList
            - 
              Name: SFTP_USERNAME
              Value: !Ref SFTPUserName

      RequiresCompatibilities:
        - EC2
      Volumes: 
        - 
          Host: 
            SourcePath: "/var/log/ecs"
          Name: "ecs-logs"
  TaskSchedule:
    Type: AWS::Events::Rule
    Properties:
      Description: Trigger ECS task to copy BPOD files from USPS
      Name: !Sub  "${ProductName}-${Region}-${ProjectName}-daily-event-rule"
      ScheduleExpression: cron(/5 * ? * * *)
      State: ENABLED
      Targets:
        - Id: !Sub "${ProductName}-${Region}-${ProjectName}-daily-event-rule-targetId"
          EcsParameters:
            LaunchType: EC2
            TaskDefinitionArn: !Ref TaskDefinition
            TaskCount: 1
          RoleArn:
            Fn::GetAtt:
            - ExecutionRole
            - Arn
          Arn: !Ref ECSClusterARN
